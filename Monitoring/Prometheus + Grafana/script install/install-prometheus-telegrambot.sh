#!/bin/bash
#
# Use: install prometheus telegram bot in ubuntu 18.04
# Use with user root
# Script by: Phạm Trung Dũng
# Website: https://toivietblog.com
# Date: 18-08-2020

apt install make -y
wget https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz
tar -xvf go1.13.3.linux-amd64.tar.gz && rm -f go1.13.3.linux-amd64.tar.gz

mv go /usr/local
export GOROOT=/usr/local/go
mv /usr/bin/go /usr/bin/go.bk
ln -s /usr/local/go/bin/go /usr/bin/go

go get github.com/inCaller/prometheus_bot
cd go/src/github.com/inCaller/prometheus_bot/
make clean
make

mv prometheus_bot /usr/local/bin/telegrambot
mkdir /etc/telegrambot
mv testdata /etc/telegrambot/
cp /etc/telegrambot/testdata/template.tmpl /etc/telegrambot/template.tmpl

cat > /etc/telegrambot/config.yaml <<"EOF"
telegram_token: "Bot-Token"
template_path: "/etc/telegrambot/template.tmpl"
time_zone: "Asia/Ho_Chi_Minh"
split_token: "|"
time_outdata: "02/01/2006 15:04:05"
split_msg_byte: 10000
EOF

useradd -rs /bin/false telegrambot
chown telegrambot:telegrambot /usr/local/bin/telegrambot
chown -R telegrambot:telegrambot /etc/telegrambot/*

cat > /etc/systemd/system/telegrambot.service <<"EOF"
[Unit]
Description=Prometheus Telegram Bot Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=telegrambot
Group=telegrambot
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/telegrambot \
-c /etc/telegrambot/config.yaml \
-l ":9087" \
-t /etc/telegrambot/template.tmpl

SyslogIdentifier=telegrambot
Restart=always

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable telegrambot.service
systemctl start telegrambot.service