#!/bin/bash
#
# Use: install vmware exporter in ubuntu 18.04, python > 3.6
# Use with user root
# Script by: Phạm Trung Dũng
# Website: https://toivietblog.com
# Date: 26-08-2020
# Github exporter: https://github.com/pryorda/vmware_exporter

python3 --version
if [ $? -ne 0 ]; then
    apt install python3 -y
fi

pip3 --version
if [ $? -ne 0 ]; then
    apt install python3-pip -y
fi

pip3 install vmware_exporter
pip3 install --ignore-installed PyYAML

cat > /usr/local/lib/python3.6/dist-packages/vmware_exporter/config.yml <<"EOF"
default:
    vsphere_host: vcenter.yourdomain.com
    vsphere_user: 'usermonitor@vcenterdomain.local'
    vsphere_password: 'usermonitorpass'
    ignore_ssl: True
    specs_size: 5000
    fetch_custom_attributes: True
    fetch_tags: True
    fetch_alarms: True
    collect_only:
        vms: True
        vmguests: True
        datastores: True
        hosts: True
        snapshots: True
EOF

cat > /etc/systemd/system/vmware_exporter.service <<"EOF"
[Unit]
Description=Prometheus VMWare Exporter
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/bin/python3 /usr/local/bin/vmware_exporter -c /usr/local/lib/python3.6/dist-packages/vmware_exporter/config.yml
SyslogIdentifier=vmware_exporter
Restart=always

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable vmware_exporter.service
systemctl start vmware_exporter.service