#!/bin/bash
#
# Use: install prometheus alertmanager in ubuntu 18.04
# Use with user root
# Script by: Phạm Trung Dũng
# Website: https://toivietblog.com
# Date: 18-08-2020

wget https://github.com/prometheus/alertmanager/releases/download/v0.21.0/alertmanager-0.21.0.linux-amd64.tar.gz
tar -xvzf alertmanager-0.21.0.linux-amd64.tar.gz && rm -f alertmanager-0.21.0.linux-amd64.tar.gz
cd alertmanager-0.21.0.linux-amd64/

mv alertmanager /usr/local/bin/
mkdir -p /etc/alertmanager/data
mv alertmanager.yml /etc/alertmanager/

useradd -rs /bin/false alertmanager
chown alertmanager:alertmanager /usr/local/bin/alertmanager
chown -R alertmanager:alertmanager /etc/alertmanager/*

cat > /lib/systemd/system/alertmanager.service <<"EOF"
[Unit]
Description=Alert Manager Service
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=alertmanager
Group=alertmanager
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/alertmanager \
--config.file=/etc/alertmanager/alertmanager.yml \
--web.listen-address=":9093" \
--storage.path=/etc/alertmanager/data

SyslogIdentifier=alertmanager
Restart=always

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable alertmanager.service
systemctl start alertmanager.service