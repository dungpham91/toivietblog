#!/bin/bash
#
# Use: install vmware exporter in ubuntu 18.04, python > 3.6
# Use with user root
# Script by: Phạm Trung Dũng
# Website: https://toivietblog.com
# Date: 26-08-2020
# Bind exporter: https://github.com/prometheus-community/bind_exporter

groupadd --system prometheus
grep prometheus /etc/group
useradd -s /sbin/nologin -r -g prometheus prometheus

apt install prometheus-bind-exporter -y

echo "" >> /etc/bind/named.conf.local
echo "statistics-channels {" >> /etc/bind/named.conf.local
echo "    inet 127.0.0.1 port 8053 allow { 127.0.0.1; };" >> /etc/bind/named.conf.local
echo "};" >> /etc/bind/named.conf.local

service bind9 reload
service prometheus-bind-exporter restart